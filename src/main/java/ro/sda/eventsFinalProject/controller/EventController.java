package ro.sda.eventsFinalProject.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.sda.eventsFinalProject.model.Event;
import ro.sda.eventsFinalProject.service.EventService;

import java.util.List;

//mesaj pentru utilizator
@RestController
public class EventController {
    private EventService eventService;

    public EventController(EventService eventService) {
        this.eventService = eventService;
    }

    //http://localhost:8080/events
    @PostMapping("events")
    public ResponseEntity createEvent(@RequestBody Event event) {//responseEntity - un tip de date -
        // pentru http -uneori trebuie sa returnam eroare, de asta returnam ResponseEntity
        if (event.getId() != null) {
            return new ResponseEntity("Id must be empty", HttpStatus.BAD_REQUEST);
        }
        try {
            Event savedEvent = eventService.saveEvent(event);
            return new ResponseEntity<>(savedEvent, HttpStatus.OK);
        } catch (IllegalArgumentException exception) {
            return new ResponseEntity(exception.getMessage(), HttpStatus.BAD_REQUEST);
        }//daca nu punem try-catch- returneaza server error
    }

    @GetMapping("/events/{id}")
    public ResponseEntity readEvent(@PathVariable(name = "id") Integer eventId) {
        try {
            Event event = eventService.readEvent(eventId);
            return new ResponseEntity<>(event, HttpStatus.OK);
        } catch (IllegalArgumentException exception) {
            return new ResponseEntity(exception.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/events")
    public ResponseEntity readAllEvents() {
        List<Event> events = eventService.readAllEvents();
        return new ResponseEntity<>(events, HttpStatus.OK);
    }

    @PutMapping("/events/{id}")
    public ResponseEntity updateEvent(@PathVariable(name = "id") Integer pathId, @RequestBody Event eventToUpdate) {
        if (!pathId.equals(eventToUpdate.getId())) {
            return new ResponseEntity<>("Inconsistent ids", HttpStatus.BAD_REQUEST);
        }
        try {
            Event upDatedEvent = eventService.updateEvent(eventToUpdate);
            return new ResponseEntity<>(upDatedEvent, HttpStatus.OK);
        } catch (IllegalArgumentException exception) {
            return new ResponseEntity<>(exception.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("/events/{id}")
    public ResponseEntity deleteEvent(@PathVariable Integer id) {//@PathVariable(name = "pathId") Integer eventId -> pathVariable leaga eventId de variabala din path
        try {
            eventService.deleteEvent(id);
            return new ResponseEntity("Event deleted successfully", HttpStatus.NO_CONTENT);
        } catch (IllegalArgumentException e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

}
