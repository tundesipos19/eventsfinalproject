package ro.sda.eventsFinalProject.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.sda.eventsFinalProject.model.Account;
import ro.sda.eventsFinalProject.service.AccountService;

import java.util.List;

@RestController
public class AccountController {
    private AccountService accountService;

    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }
    @PostMapping("/authentication/register")
    public ResponseEntity createAccount(@RequestBody Account account){
        if(account.getId() != null){
            return new ResponseEntity("Id must be empty- it was generated!", HttpStatus.BAD_REQUEST);
        }
        try {
            System.out.println(account);
            Account savedAccount = accountService.saveAccount(account);
            return new ResponseEntity(savedAccount,HttpStatus.OK);
        }
        catch (IllegalArgumentException e){
            return new ResponseEntity(e.getMessage(),HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/authentication/login/{id}")
    public ResponseEntity readAccount(@PathVariable Integer id){
        try {
            Account account = accountService.readAccount(id);
            return new ResponseEntity(account,HttpStatus.FOUND);
        }
        catch (IllegalArgumentException e){
            return new ResponseEntity(e.getMessage(),HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/accounts")
    public ResponseEntity readAllAccount(){
        List<Account> accounts = accountService.readAllAccount();
        return new ResponseEntity(accounts,HttpStatus.OK);
    }

    @PutMapping("accounts/{id}")
    public ResponseEntity updateAccount(@PathVariable (name = "id") Integer pathId, @RequestBody Account accountToUpdate){
        if(!pathId.equals(accountToUpdate.getId())){
            return new ResponseEntity("Inconsistent ids",HttpStatus.BAD_REQUEST);
        }
        try {
            Account updatedAccount = accountService.updateAccount(accountToUpdate);
            return new ResponseEntity(updatedAccount,HttpStatus.OK);
        }
        catch (IllegalArgumentException e){
            return new ResponseEntity(e.getMessage(),HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("account/{id}")
    public ResponseEntity deleteAccount(@PathVariable Integer id){
        try {
            accountService.deleteAccount(id);
            return new ResponseEntity("Account was deleted successfully.",HttpStatus.OK);
        }
        catch (IllegalArgumentException e){
            return new ResponseEntity(e.getMessage(),HttpStatus.BAD_REQUEST);
        }
    }
}
