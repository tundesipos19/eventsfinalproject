package ro.sda.eventsFinalProject.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.sda.eventsFinalProject.model.Category;
import ro.sda.eventsFinalProject.model.Event;
import ro.sda.eventsFinalProject.service.CategoryService;

import java.util.List;

@RestController
public class CategoryController {
    private CategoryService categoryService;
    public CategoryController(CategoryService categoryService){
        this.categoryService = categoryService;
    }
    @PostMapping("/categories")
    public ResponseEntity createCategory(@RequestBody Category category){
        if(category.getId() !=null){
            return new ResponseEntity("Id must be empty! ", HttpStatus.BAD_REQUEST);
        }
        try{
            Category savedCategory = categoryService.saveCategory(category);
            return new ResponseEntity(savedCategory,HttpStatus.OK);
        }
        catch (IllegalArgumentException e){
            return new ResponseEntity(e.getMessage(),HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/categories/{id}")
    public ResponseEntity readCategory(@PathVariable Integer id){
        try{
            Category category = categoryService.readCategory(id);
            return new ResponseEntity(category,HttpStatus.OK);
        }
        catch (IllegalArgumentException e) {
            return new ResponseEntity(e.getMessage(),HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/categories")
    public ResponseEntity readAllCategory(){
        List<Category> categories = categoryService.readAllCategory();
        return new ResponseEntity(categories,HttpStatus.OK);
    }

    @PutMapping("/categories/{id}")
    public ResponseEntity updateCategory(@PathVariable (name = "id") Integer pathId, @RequestBody Category categoryToUpdate){
        if(!pathId.equals(categoryToUpdate.getId())){
            return new ResponseEntity<>("Inconsistent ids", HttpStatus.BAD_REQUEST);
        }
        try {
            Category updatedCategory = categoryService.updateCategory(categoryToUpdate);
            return new ResponseEntity<>(updatedCategory,HttpStatus.OK);
        }
        catch (IllegalArgumentException e){
            return new ResponseEntity<>(e.getMessage(),HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("/categories/{id}")
    public ResponseEntity deleteCategory(@PathVariable Integer id){
        try{
            categoryService.deleteCategory(id);
            return new ResponseEntity("Category was deleted successfully.",HttpStatus.NO_CONTENT);
        }
        catch (IllegalArgumentException e){
            return new ResponseEntity(e.getMessage(),HttpStatus.BAD_REQUEST);
        }
    }
    @GetMapping("/categories/events-of-category/{id}")
    public ResponseEntity eventsOfCategory(@PathVariable Integer id){
        try{
            List<Event> events = categoryService.eventsOfCategory(id);
            return new ResponseEntity(events,HttpStatus.OK);
        }
        catch (IllegalArgumentException e){
            return new ResponseEntity<>(e.getMessage(),HttpStatus.BAD_REQUEST);
        }
    }
}
