package ro.sda.eventsFinalProject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ro.sda.eventsFinalProject.model.Account;

@Repository
public interface AccountRepository extends JpaRepository<Account, Integer> {
}
