package ro.sda.eventsFinalProject.repository;

import org.apache.el.stream.Optional;
import org.hibernate.mapping.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ro.sda.eventsFinalProject.model.Event;
@Repository
public interface EventRepository extends JpaRepository<Event,Integer> {
//    List<Event> findAllByOrderedByName();
//    @Query("select e from Event e where e.category= e.category")
//    Optional<Event> findAllByCategory(@Param ("category") String name;
}
