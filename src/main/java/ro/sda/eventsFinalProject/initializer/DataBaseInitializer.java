package ro.sda.eventsFinalProject.initializer;

import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ro.sda.eventsFinalProject.model.Account;
import ro.sda.eventsFinalProject.model.Category;
import ro.sda.eventsFinalProject.model.Event;
import ro.sda.eventsFinalProject.service.AccountService;
import ro.sda.eventsFinalProject.service.CategoryService;
import ro.sda.eventsFinalProject.service.EventService;

import java.time.LocalDateTime;

@Component
public class DataBaseInitializer {
    private final EventService eventService;
    private final CategoryService categoryService;
    private final AccountService accountService;

    public DataBaseInitializer(EventService eventService, CategoryService categoryService, AccountService accountService) {
        this.eventService = eventService;
        this.categoryService = categoryService;
        this.accountService = accountService;
    }

    @EventListener(ApplicationReadyEvent.class)
    public void initDataBase() {
        Account admin = new Account(null, "tundesipos19@gmail.com", "Tunde", "Sipos", "abc123", "abc123");
        accountService.saveAccount(admin);

        Category c1 = new Category(null,
                "Sports",
                "Sports are also a kind of events with great economic and other potentialities." +
                        " Such events range from small like local level football competition to large like FIFA Worldcup.",
                null
        );
        categoryService.saveCategory(c1);

        Category c2 = new Category(null,
                "Educational events",
                "These are events that are designed to educate or inform people about a particular topic or subject, such as lectures, workshops, and seminars.\n",
                null
        );
        categoryService.saveCategory(c2);

        Category c3 = new Category(null,
                "Lifecycle & Milestones",
                ": Events like birthdays, marriage, death ceremony, etc.\n",
                null
        );
        categoryService.saveCategory(c3);

        Event e1 = new Event(null,
                "2024 Summer Olympics",
                LocalDateTime.of(2024, 7, 26, 10, 0, 0),
                LocalDateTime.of(2024, 8, 11, 22, 00, 00),
                "The 2024 Summer Olympics is a forthcoming international multi-sport event. The Games will feature " +
                        "the debut of breaking (also known as breakdancing) as an Olympic event.",
                "Paris",
                "https://upload.wikimedia.org/wikipedia/en/thumb/d/d1/2024_Summer_Olympics_logo.svg/330px-2024_Summer_Olympics_logo.svg.png",
                c1);
        eventService.saveEvent(e1);

        Event e2 = new Event(
                null,
                "Online Teaching Conference",
                LocalDateTime.of(2023, 6, 21, 20, 0, 0),
                LocalDateTime.of(2023, 6, 23, 23, 59, 59),
                "The Online Teaching Conference is a gathering of faculty, staff, and administrators who are leading the way" +
                        " in developing innovative and effective online education.",
                "California",
                "https://onlineteachingconference.org/wp-content/uploads/2023/07/Logo2024.jpg",
                c2);
        eventService.saveEvent(e2);

        Event e3 = new Event(null,
                "Untold",
                LocalDateTime.of(2023, 8, 3, 21, 00, 00),
                LocalDateTime.of(2023, 8, 6, 1, 00, 00),
                "Join the largest festival from the seaside. Join Untold. More than 200 artists creating an amazing atmosphere in Cluj-Napoca, Romania",
                "Cluj- Napoca",
                "https://static.infomusic.ro/media/2016/11/afis-untold-festival-2017-2.jpg",
                c1);
        eventService.saveEvent(e3);
    }
}
