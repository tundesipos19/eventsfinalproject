package ro.sda.eventsFinalProject.service;

import org.springframework.stereotype.Service;
import ro.sda.eventsFinalProject.model.Account;
import ro.sda.eventsFinalProject.repository.AccountRepository;

import java.util.List;
import java.util.NoSuchElementException;

@Service
public class AccountService {
    private final AccountRepository accountRepository;

    public AccountService(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    public Account saveAccount(Account accountToBeSaved) {
        if (accountToBeSaved == null) {
            throw new IllegalArgumentException("An account must have a body!");
        }
        if (accountToBeSaved.getEmail() == null) {
            throw new IllegalArgumentException("An account must have an email!");
        }
        if (accountToBeSaved.getFirstName() == null) {
            throw new IllegalArgumentException("An account must have a firstname!");
        }
        if (accountToBeSaved.getLastName() == null) {
            throw new IllegalArgumentException("An account must have a lastname!");
        }
        if (accountToBeSaved.getPassword() == null) {
            throw new IllegalArgumentException("An account must have a password!");
        }
        if (accountToBeSaved.getReTypePassword() == null){
            throw new IllegalArgumentException("Please, retype your password!");
        }
        if(!accountToBeSaved.getPassword().equals(accountToBeSaved.getReTypePassword())){
            throw new IllegalArgumentException("Passwords are different!");
        }
        else {
            Account savedAccount = accountRepository.save(accountToBeSaved);
            return savedAccount;
        }


    }

    public Account readAccount(Integer id) {
        if (id == null) {
            throw new IllegalArgumentException("Account id must not be null!");
        }
        try {
            Account account = accountRepository.findById(id).get();
            return account;
        } catch (NoSuchElementException e) {
            throw new IllegalArgumentException("There is no account with id: " + id);
        }
    }

    public List<Account> readAllAccount() {
        return accountRepository.findAll();
    }

    public Account updateAccount(Account updatedAccount){
        if(updatedAccount == null){
            throw new IllegalArgumentException("An account must have a body!");
        }
        Account accountToUpdate = readAccount(updatedAccount.getId());
        accountRepository.save(updatedAccount);
        return accountToUpdate;
    }

    public void deleteAccount(Integer accountId){
        Account accountToDelete = readAccount(accountId);
        accountRepository.delete(accountToDelete);
    }
}
