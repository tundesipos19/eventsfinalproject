package ro.sda.eventsFinalProject.service;

import org.springframework.stereotype.Service;
import ro.sda.eventsFinalProject.model.Event;
import ro.sda.eventsFinalProject.repository.EventRepository;

import java.util.List;
import java.util.NoSuchElementException;
//mesaj pentru programator - daca numai aruncam exceptia fara sa- o rezolvam, utilizatorul nu vede care e exceptia

@Service
public class EventService {
    private final EventRepository eventRepository;

    public EventService(EventRepository eventRepository) {
        this.eventRepository = eventRepository;
    }

    public Event saveEvent(Event eventToBeSaved) {//C
        if (eventToBeSaved == null) {
            throw new IllegalArgumentException("An event must have body.");
        }
        if (eventToBeSaved.getName() == null) {
            throw new IllegalArgumentException("An event must have a name");
        }

        if (eventToBeSaved.getStartDate() == null || eventToBeSaved.getEndDate() == null || eventToBeSaved.getStartDate().isAfter(eventToBeSaved.getEndDate())) {
            throw new IllegalArgumentException("Event' s date is incorrect!");
        }
        if (eventToBeSaved.getCategory() == null) {
            throw new IllegalArgumentException("An event must have a category");
        } else {
            Event savedEvent = eventRepository.save(eventToBeSaved);
            return savedEvent;
        }
    }

    public Event readEvent(Integer id) {//R
        if (id == null) {
            throw new IllegalArgumentException("Event id must not be null!");
        }
        try {
            Event event = eventRepository.findById(id).get();
            return event;
        } catch (NoSuchElementException e) {
            throw new IllegalArgumentException("There is no event with Id " + id);
        }
    }

    public List<Event> readAllEvents() {
        return eventRepository.findAll();
    }

    public Event updateEvent(Event updatedEvent) {//U
        if (updatedEvent == null) {
            throw new IllegalArgumentException("An event must have body.");
        }
        //verivicam daca exista un eveniment cu id- ul dat din baza de date
        Event eventToUpdate = readEvent(updatedEvent.getId());
        eventRepository.save(updatedEvent);
        return eventToUpdate;
    }

    public void deleteEvent(Integer eventID) {//D
        Event eventToDelete = readEvent(eventID);
        eventRepository.delete(eventToDelete);
    }
}
