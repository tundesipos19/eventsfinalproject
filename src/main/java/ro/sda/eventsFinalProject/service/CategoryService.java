package ro.sda.eventsFinalProject.service;

import org.springframework.stereotype.Service;
import ro.sda.eventsFinalProject.model.Category;
import ro.sda.eventsFinalProject.repository.CategoryRepository;
import ro.sda.eventsFinalProject.model.Event;

import java.util.List;
import java.util.NoSuchElementException;

@Service
public class
CategoryService {
    private final CategoryRepository categoryRepository;

    public CategoryService(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    public Category saveCategory(Category categoryToSave){
        if(categoryToSave == null){
            throw new IllegalArgumentException("A category must have a body!");
        }
        if(categoryToSave.getName() == null){
            throw new IllegalArgumentException("A category must have a name!");
        }
        else {
            Category savedCategory = categoryRepository.save(categoryToSave);
            return savedCategory;
        }
    }

    public Category readCategory(Integer id){
        if(id == null){
            throw new IllegalArgumentException("Category id must not be null!");
        }
        try{
            Category category = categoryRepository.findById(id).get();
            return category;
        }
        catch (NoSuchElementException e){
            throw new IllegalArgumentException("There is no category with Id: " +id);
        }
    }

    public List<Category> readAllCategory(){
        return categoryRepository.findAll();
    }

    public Category updateCategory(Category updatedCategory) {
        if (updatedCategory == null) {
            throw new IllegalArgumentException("A category must have body!");
        }
        Category categoryToUpdate = readCategory(updatedCategory.getId());
        categoryRepository.save(updatedCategory);
        return categoryToUpdate;
    }
    public void deleteCategory(Integer categoryId){
        Category categoryToDelete = readCategory(categoryId);
        categoryRepository.delete(categoryToDelete);
    }
    public List<Event> eventsOfCategory(Integer categoryId){
        Category category = readCategory(categoryId);
        if(category.getEvents().isEmpty()){
            throw new IllegalArgumentException("There is no events in this category.");
        }
        return category.getEvents();
    }
}
